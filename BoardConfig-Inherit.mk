#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

DANDELIONSEPOLICY_PATH := device/xiaomi/dandelion-sepolicy

BOARD_VENDOR_SEPOLICY_DIRS += $(DANDELIONSEPOLICY_PATH)/sepolicy/vendor
BOARD_PLAT_PRIVATE_SEPOLICY_DIRS += $(DANDELIONSEPOLICY_PATH)/sepolicy/private

